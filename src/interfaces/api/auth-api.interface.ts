export interface IAuthSignupReq {
    username: string;
    password: string;
}

export interface IAuthLoginReq {
    username: string;
    password: string;
}

export interface IAuthLoginRes {
    token: string;
}
