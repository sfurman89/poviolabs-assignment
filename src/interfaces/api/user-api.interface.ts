export interface IUserGetRes {
    username: string;
}

export interface IUserUpdatePasswordReq {
    password: string;
}

export interface IUserLikeReq {
    id?: number;
}

export interface IUserUnlikeReq {
    id?: number;
}

export interface IUserNumberOfLikesReq {
    id?: number;
}

export interface IUserNumberOfLikesRes {
    username?: string;
    numberOfLikes?: number;
}
