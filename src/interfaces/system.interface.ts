import express from 'express';
import { Context } from '../context';

export interface IRequest extends express.Request {
    context: Context;
    body: { [key: string]: any };
}

export interface IResponse extends express.Response {
    success: (data?: any, message?: string, code?: number, status?: boolean) => void;
    error: (message?: string, statusCode?: number, code?: number, data?: any, status?: boolean) => void;
}

export interface INextFunction extends express.NextFunction {
}
