import { Handler } from '../types/system.type';

export interface IRouteOption {
    path: string;
    method: string;
    auth?: boolean;
    parent?: string;
    handler: Handler | Handler[];
}

export interface IRoute {
    path: string;
    routes: IRouteOption[];
}

export interface IToken {
    id: number;
    iat: number;
    exp: number;
}

export interface IApiResponse {
    status: boolean;
    code: number;
    data: any;
    message: string;
}
