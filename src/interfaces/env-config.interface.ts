export interface IEnvConfig {
    http: {
        host: string;
        port: number;
    };
}
