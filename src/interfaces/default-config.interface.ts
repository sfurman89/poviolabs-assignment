export interface IDefaultConfig {
    swagger: {
        definition: {
            openapi: string;
            info: {
                title: string;
                description: string;
                version: string;
            };
        };
        options: {
            apis: string[];
        };
    };
}
