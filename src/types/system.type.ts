import { Application } from 'express';
import { INextFunction, IRequest, IResponse } from '../interfaces/system.interface';

export type Wrapper = ((app: Application) => void);
export type Handler = (req: IRequest, res: IResponse, next: INextFunction) => Promise<void> | void;
