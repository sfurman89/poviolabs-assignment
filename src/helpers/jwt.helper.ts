import { IToken } from '../interfaces/app.interface';
import jwt from 'jsonwebtoken';
import moment from 'moment';

export class JwtHelper {
    /**
     * @static
     * @method CreateToken
     * @param {string|object} payload
     * @returns {string}
     */
    static CreateToken = (payload: string | object): string => jwt.sign(payload, process.env.JWT_SECRET, {
        expiresIn: '24h'
    });

    /**
     * @static
     * @method GetToken
     * @param {string} token
     * @returns {string}
     */
    static GetToken = (token: string): IToken => jwt.decode(token.slice(7)) as IToken;

    /**
     * @static
     * @method VerifyToken
     * @param {string} token
     * @returns {string}
     */
    static VerifyToken = (token: string): boolean => (jwt.decode(token.slice(7)) as IToken).exp >= moment().unix();
}

