export class MainHelper {
    /**
     * @static
     * @method Delay
     * @param {number} ms
     * @returns {Promise<void>}
     */
    static Delay = (ms: number = 1000): Promise<void> => new Promise<void>(resolve => setTimeout(resolve, ms));
}

