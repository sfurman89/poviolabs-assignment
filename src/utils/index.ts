import { Application } from 'express';
import { IRoute } from '../interfaces/app.interface';
import { Wrapper } from '../types/system.type';
import { INextFunction, IRequest, IResponse } from '../interfaces/system.interface';
import { JwtHelper } from '../helpers/jwt.helper';
import { ExceptionHandler } from '../handlers/exception.handler';
import { ResponseCodeEnum } from '../enums/response-code.enum';

/**
 * @function injectMiddlewares
 * @param {Wrapper[]} middlewares
 * @param {Application} app
 */
export const injectMiddlewares = (middlewares: Wrapper[], app: Application) => {
    for (const middleware of middlewares) {
        middleware(app);
    }
};

/**
 * @function injectRoutes
 * @param {IRoute[]} routes
 * @param {Application} app
 */
export const injectRoutes = (routes: IRoute[], app: Application) => {
    app.use('/*', (req: IRequest, res: IResponse, next: INextFunction) => {
        for (const controller of routes) {
            for (const route of controller.routes) {
                if (req.baseUrl === `${route.parent || ''}${route.path}` || _checkUrlBySegments(req.baseUrl, `${route.parent || ''}${route.path}`)) {
                    if (typeof route.auth === 'undefined' || route.auth) {
                        const authorization = req.headers.authorization;

                        if (!authorization) {
                            throw new ExceptionHandler('Missing authorization token', ResponseCodeEnum.UNAUTHORIZED, 401);
                        }

                        if (!authorization.startsWith('Bearer ')) {
                            throw new ExceptionHandler('Invalid authorization token', ResponseCodeEnum.UNAUTHORIZED, 401);
                        }

                        if (!JwtHelper.VerifyToken(authorization)) {
                            throw new ExceptionHandler('Authorization token has expired', ResponseCodeEnum.UNAUTHORIZED, 401);
                        }
                    }

                    break;
                }
            }
        }

        next();
    });

    for (const controller of routes) {
        for (const route of controller.routes) {
            const { method, path, handler } = route;

            (app as any)[method](`${(controller.path)}${path}`, tryCatchMiddleware(handler));
        }
    }
};

/**
 * @private
 * @function tryCatchMiddleware
 * @param handler
 * @returns {(req: IRequest, res: IResponse, next: INextFunction) => Promise<void>}
 */
export const tryCatchMiddleware = (handler) => {
    return async (req: IRequest, res: IResponse, next: INextFunction) => {
        try {
            await handler(req, res);
        } catch (exception) {
            next(exception);
        }
    };
};

/**
 * @private
 * @function _checkUrlBySegments
 * @param {string} baseUrl
 * @param {string} path
 * @returns {boolean}
 */
const _checkUrlBySegments = (baseUrl: string, path: string): boolean => {
    const baseUrlSplit = baseUrl.split('/');
    const pathSplit = path.split('/');
    const length = pathSplit.length;

    for (let i = 0; i < length; i++) {
        if (pathSplit[i].includes(':')) {
            continue;
        }

        if (pathSplit[i] !== baseUrlSplit[i]) {
            return false;
        }
    }

    return true;
};
