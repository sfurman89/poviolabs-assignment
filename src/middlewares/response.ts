import { IApiResponse } from '../interfaces/app.interface';

/**
 * @function successResponse
 * @param {any} data
 * @param {string} message
 * @param {number} code
 * @param {boolean} status
 * @returns {IApiResponse}
 */
export const successResponse = (data?: any, message?: string, code?: number, status?: boolean): IApiResponse => {
    return {
        status: status || true,
        code: code || 200200,
        data: data || null,
        message: message || ''
    };
};

/**
 * @function errorResponse
 * @param {string} message
 * @param {number} code
 * @param {any} data
 * @param {boolean} status
 * @returns {IApiResponse}
 */
export const errorResponse = (message: string, code?: number, data?: any, status?: boolean): IApiResponse => {
    return {
        status: status || false,
        code: code || 400400,
        data: data || null,
        message: message || ''
    };
};
