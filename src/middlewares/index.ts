import { injectBodyParser, injectCompression, injectCors, injectResponse, injectSwagger } from './common';

export default [injectCors, injectBodyParser, injectCompression, injectResponse, injectSwagger];
