import cors from 'cors';
import parser from 'body-parser';
import compression from 'compression';
import { Application, RequestHandler } from 'express';
import { INextFunction, IRequest, IResponse } from '../interfaces/system.interface';
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import defaultConfig from '../config/default.config';
import { errorResponse, successResponse } from './response';

/**
 * @function injectCors
 * @param {Application} app
 */
export const injectCors = (app: Application): void => {
    app.use(cors({ credentials: true, origin: true }));
};

/**
 * @function injectBodyParser
 * @param {Application} app
 */
export const injectBodyParser = (app: Application): void => {
    app.use(parser.urlencoded({ extended: true }));
    app.use(parser.json());
};

/**
 * @function injectCompression
 * @param {Application} app
 */
export const injectCompression = (app: Application): void => {
    app.use(compression());
};

/**
 * @function injectResponse
 * @param {Application} app
 */
export const injectResponse = (app: Application): void => {
    app.use(_createSuccessResponse());
    app.use(_createErrorResponse());
};

/**
 * @function injectSwagger
 * @param {Application} app
 */
export const injectSwagger = (app: Application): void => {
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(_getSwaggerConfig()));
};

/**
 * @private
 * @function _createSuccessResponse
 * @returns {RequestHandler}
 */
const _createSuccessResponse = (): RequestHandler => {
    return (req: IRequest, res: IResponse, next: INextFunction) => {
        res.success = (data?: any, message?: string, code?: number, status?: boolean) => {
            res.send(successResponse(data, message, code, status));
        };

        next();
    };
};

/**
 * @private
 * @function _createErrorResponse
 * @returns {RequestHandler}
 */
const _createErrorResponse = (): RequestHandler => {
    return (req: IRequest, res: IResponse, next: INextFunction) => {
        res.error = (message?: string, statusCode?: number, code?: number, data?: any, status?: boolean) => {
            res.status(statusCode).send(errorResponse(message, code, data, status));
        };

        next();
    };
};

/**
 * @private
 * @function _getSwaggerConfig
 * @returns {object}
 */
const _getSwaggerConfig = (): object => {
    const swaggerDefinition = defaultConfig.swagger.definition;
    const options: any = defaultConfig.swagger.options;
    options.swaggerDefinition = swaggerDefinition;

    return swaggerJSDoc(options);
};
