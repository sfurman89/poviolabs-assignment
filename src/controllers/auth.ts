import { IAuthLoginReq, IAuthLoginRes, IAuthSignupReq } from '../interfaces/api/auth-api.interface';
import { UserEntity } from '../models/entity/user.entity';
import { ExceptionHandler } from '../handlers/exception.handler';
import { IRequest } from '../interfaces/system.interface';
import { JwtHelper } from '../helpers/jwt.helper';
import md5 from 'md5';
import * as _ from 'lodash';

/**
 * @function signup
 * @param {IRequest} req
 * @returns {Promise<void>}
 */
export const signup = async (req: IRequest): Promise<void> => {
    const params = req.body as IAuthSignupReq;

    if (_.keys(params).length !== 2 || !params.username || !params.password) {
        throw new ExceptionHandler('Invalid request parameters');
    }

    let user: UserEntity = await req.context.connection.getRepository(UserEntity).findOne({
        username: params.username
    });

    if (user) {
        throw new ExceptionHandler('User already exist');
    }

    user = new UserEntity();
    user.username = params.username;
    user.password = md5(params.password);

    await req.context.connection.getRepository(UserEntity).save(user);
};

/**
 * @function login
 * @param {IRequest} req
 * @returns {Promise<IAuthLoginRes>}
 */
export const login = async (req: IRequest): Promise<IAuthLoginRes> => {
    const params = req.body as IAuthLoginReq;

    if (_.keys(params).length !== 2 || !params.username || !params.password) {
        throw new ExceptionHandler('Invalid request parameters');
    }

    const user: UserEntity = await req.context.connection.getRepository(UserEntity).findOne({
        username: params.username,
        password: md5(params.password)
    }, {
        select: ['id', 'password']
    });

    if (!user) {
        throw new ExceptionHandler('Invalid username or password');
    }

    return {
        token: JwtHelper.CreateToken({ id: user.id })
    };
};
