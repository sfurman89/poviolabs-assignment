import { JwtHelper } from '../helpers/jwt.helper';
import { IRequest } from '../interfaces/system.interface';
import {
    IUserGetRes,
    IUserLikeReq,
    IUserNumberOfLikesReq,
    IUserNumberOfLikesRes,
    IUserUnlikeReq,
    IUserUpdatePasswordReq
} from '../interfaces/api/user-api.interface';
import { UserEntity } from '../models/entity/user.entity';
import { ExceptionHandler } from '../handlers/exception.handler';
import * as _ from 'lodash';
import md5 from 'md5';
import { UserLikeEntity } from '../models/entity/user-like.entity';
import { getManager } from 'typeorm';
import { classToPlain } from 'class-transformer';

/**
 * @function get
 * @param {IRequest} req
 * @returns {Promise<IUserGetRes>}
 */
export const get = async (req: IRequest): Promise<IUserGetRes> => {
    const id = JwtHelper.GetToken(req.headers.authorization).id;
    const user: UserEntity = await req.context.connection.getRepository(UserEntity).findOne({
        id
    }, {
        select: ['username']
    });

    if (!user) {
        throw new ExceptionHandler('User does not exist');
    }

    return { ...user };
};

/**
 * @function updatePassword
 * @param {IRequest} req
 * @returns {Promise<void>}
 */
export const updatePassword = async (req: IRequest): Promise<void> => {
    const params = req.body as IUserUpdatePasswordReq;

    if (_.keys(params).length !== 1 || !params.password) {
        throw new ExceptionHandler('Invalid request parameters');
    }

    const id = JwtHelper.GetToken(req.headers.authorization).id;
    const user: UserEntity = await req.context.connection.getRepository(UserEntity).findOne({
        id
    });

    if (!user) {
        throw new ExceptionHandler('User does not exist');
    }

    user.password = md5(params.password);

    await req.context.connection.getRepository(UserEntity).save(user);
};

/**
 * @function like
 * @param {IRequest} req
 * @returns {Promise<void>}
 */
export const like = async (req: IRequest): Promise<void> => {
    const params: IUserLikeReq = req.params;

    if (!+params.id) {
        throw new ExceptionHandler('Id must be integer');
    }

    const id = JwtHelper.GetToken(req.headers.authorization).id;

    if (id === +params.id) {
        throw new ExceptionHandler('You can not like yourself');
    }

    const user: UserEntity = await req.context.connection.getRepository(UserEntity).findOne({
        id: params.id
    }, {
        select: ['username']
    });

    if (!user) {
        throw new ExceptionHandler('User does not exist');
    }

    let userLike: UserLikeEntity = await req.context.connection.getRepository(UserLikeEntity).findOne({
        userId: id,
        userIdLike: params.id
    }, {
        select: ['id']
    });

    if (userLike) {
        throw new ExceptionHandler('You have already liked');
    }

    userLike = new UserLikeEntity();
    userLike.userId = id;
    userLike.userIdLike = params.id;

    await req.context.connection.getRepository(UserLikeEntity).save(userLike);
};

/**
 * @function unlike
 * @param {IRequest} req
 * @returns {Promise<void>}
 */
export const unlike = async (req: IRequest): Promise<void> => {
    const params: IUserUnlikeReq = req.params;

    if (!+params.id) {
        throw new ExceptionHandler('Id must be integer');
    }

    const id = JwtHelper.GetToken(req.headers.authorization).id;

    if (id === +params.id) {
        throw new ExceptionHandler('You can not unlike yourself');
    }

    const user: UserEntity = await req.context.connection.getRepository(UserEntity).findOne({
        id: params.id
    }, {
        select: ['username']
    });

    if (!user) {
        throw new ExceptionHandler('User does not exist');
    }

    const userLike: UserLikeEntity = await req.context.connection.getRepository(UserLikeEntity).findOne({
        userId: id,
        userIdLike: params.id
    }, {
        select: ['id']
    });

    if (!userLike) {
        throw new ExceptionHandler('You have not liked yet');
    }

    await req.context.connection.getRepository(UserLikeEntity).delete(userLike);
};

/**
 * @function numberOfLikes
 * @param {IRequest} req
 * @returns {Promise<IUserNumberOfLikesRes>}
 */
export const numberOfLikes = async (req: IRequest): Promise<IUserNumberOfLikesRes> => {
    const params: IUserNumberOfLikesReq = req.params;

    if (!+params.id) {
        throw new ExceptionHandler('Id must be integer');
    }

    const user: UserEntity = await req.context.connection.getRepository(UserEntity).findOne({
        id: +params.id
    }, {
        select: ['username']
    });

    if (!user) {
        throw new ExceptionHandler('Invalid user id');
    }

    const userLike = await req.context.connection.getRepository(UserLikeEntity).findAndCount({
        userIdLike: +params.id
    });

    return {
        username: user.username,
        numberOfLikes: userLike[1] || 0
    };
};

/**
 * @function mostLiked
 * @returns {Promise<IUserNumberOfLikesRes>}
 */
export const mostLiked = async (): Promise<IUserNumberOfLikesRes[]> => {
    return classToPlain(await getManager().query('SELECT user.username, COUNT(user_like.id) AS numberOfLikes FROM user_like INNER JOIN user ON(user_like.user_id_like = user.id) GROUP BY user_like.user_id_like ORDER BY numberOfLikes DESC')) as IUserNumberOfLikesRes[];
};

