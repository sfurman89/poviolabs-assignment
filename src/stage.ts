import { IEnvConfig } from './interfaces/env-config.interface';
import { Connection } from 'typeorm';
import { IDatabaseConfig } from './interfaces/database-config.interface';
import { IDefaultConfig } from './interfaces/default-config.interface';
import { closeConnection, getConnection } from './lib/mysql';

export class Stage {
    public connection: Connection;
    public envConfig: IEnvConfig;
    public databaseConfig: IDatabaseConfig;
    public defaultConfig: IDefaultConfig;

    /**
     * @constructor
     * @param {IEnvConfig} envConfig
     * @param {IDatabaseConfig} databaseConfig
     * @param {IDefaultConfig} defaultConfig
     */
    constructor(envConfig: IEnvConfig, databaseConfig: IDatabaseConfig, defaultConfig: IDefaultConfig) {
        this.envConfig = envConfig;
        this.databaseConfig = databaseConfig;
        this.defaultConfig = defaultConfig;
    }

    /**
     * @method connect
     * @returns {Promise<void>}
     */
    public async connect(): Promise<void> {
        this.connection = await getConnection(this.databaseConfig);
    }

    /**
     * @method connect
     * @returns {Promise<void>}
     */
    public async disconnect(): Promise<void> {
        await closeConnection();

        this.connection = undefined;
    }
}
