import request from 'supertest';
import jwt from 'jsonwebtoken';
import { errorResponse, successResponse } from '../../../middlewares/response';
import { startWebServer } from '../../../web-server';

let httpServer;

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();
});

afterAll(async () => {
    await httpServer.close();
});

describe('Login - API endpoint: POST /login', () => {
    test('Successfully logged in', async done => {
        request(httpServer.app)
            .post('/login')
            .send({
                username: 'user-1',
                password: 'admin'
            })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                const { sign } = jwt as jest.Mocked<typeof import('jsonwebtoken')>;

                expect(JSON.parse(res.text)).toEqual(successResponse({
                    token: sign({ id: 1 }, process.env.JWT_SECRET, {
                        expiresIn: '24h'
                    })
                }));

                done();
            });
    });

    test('Invalid username or password', async done => {
        request(httpServer.app)
            .post('/login')
            .send({
                username: 'user-1',
                password: 'admin1'
            })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('Invalid username or password'));

                done();
            });
    });

    test('Invalid request parameters', async done => {
        request(httpServer.app)
            .post('/login')
            .send({
                username: 'user-1'
            })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('Invalid request parameters'));

                done();
            });
    });
});
