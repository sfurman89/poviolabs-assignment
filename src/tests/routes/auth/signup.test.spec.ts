import request from 'supertest';
import moment from 'moment';
import { errorResponse, successResponse } from '../../../middlewares/response';
import { startWebServer } from '../../../web-server';

let httpServer;

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();
});

afterAll(async () => {
    await httpServer.close();
});

describe('Signup - API endpoint: POST /signup', () => {
    test('Creating new user', async done => {
        request(httpServer.app)
            .post('/signup')
            .send({
                username: `user${moment().unix()}`,
                password: 'admin'
            })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(successResponse(null, 'User successfully created'));

                done();
            });
    });

    test('User already exist', async done => {
        request(httpServer.app)
            .post('/signup')
            .send({
                username: 'user-1',
                password: 'admin'
            })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('User already exist'));

                done();
            });
    });

    test('Invalid request parameters', async done => {
        request(httpServer.app)
            .post('/signup')
            .send({
                username: 'user-1'
            })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('Invalid request parameters'));

                done();
            });
    });
});
