import request from 'supertest';
import jwt from 'jsonwebtoken';
import { errorResponse, successResponse } from '../../../middlewares/response';
import { startWebServer } from '../../../web-server';

let httpServer;
let token = '';

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();

    const { sign } = jwt as jest.Mocked<typeof import('jsonwebtoken')>;

    token = sign({ id: 1 }, process.env.JWT_SECRET, {
        expiresIn: '24h'
    });
});

afterAll(async () => {
    await httpServer.close();
});

describe('User - API endpoint: GET /user/:id', () => {
    test('Id must be integer', async done => {
        request(httpServer.app)
            .get('/user/id')
            .auth(token, { type: 'bearer' })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('Id must be integer'));

                done();
            });
    });

    test('Invalid user id', async done => {
        request(httpServer.app)
            .get('/user/-1')
            .auth(token, { type: 'bearer' })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('Invalid user id'));

                done();
            });
    });

    test('List username & number of likes of a user (with authorization token)', async done => {
        request(httpServer.app)
            .get('/user/2')
            .auth(token, { type: 'bearer' })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(successResponse({
                    username: 'user-2',
                    numberOfLikes: 1
                }));

                done();
            });
    });

    test('List username & number of likes of a user (without authorization token)', async done => {
        request(httpServer.app)
            .get('/user/1')
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(successResponse({
                    username: 'user-1',
                    numberOfLikes: 0
                }));

                done();
            });
    });
});
