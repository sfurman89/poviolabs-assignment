import request from 'supertest';
import jwt from 'jsonwebtoken';
import { errorResponse, successResponse } from '../../../middlewares/response';
import { ResponseCodeEnum } from '../../../enums/response-code.enum';
import { startWebServer } from '../../../web-server';

let httpServer;
let token = '';
let tokenNoUser;
const tokenExpired = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjA3MzA4MjA3LCJleHAiOjE2MDczOTQ2MDd9.z63yS_l21nc6rGx3JlwPTNBo9gnDXAwFpjlMj0dpioc';

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();

    const { sign } = jwt as jest.Mocked<typeof import('jsonwebtoken')>;

    token = sign({ id: 1 }, process.env.JWT_SECRET, {
        expiresIn: '24h'
    });

    tokenNoUser = sign({ id: -1 }, process.env.JWT_SECRET, {
        expiresIn: '24h'
    });
});

afterAll(async () => {
    await httpServer.close();
});

describe('User - API endpoint: GET /me', () => {
    test('Username of logged in user', async done => {
        request(httpServer.app)
            .get('/me')
            .auth(token, { type: 'bearer' })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(successResponse({
                    username: 'user-1'
                }));

                done();
            });
    });

    test('User does not exist', async done => {
        request(httpServer.app)
            .get('/me')
            .auth(tokenNoUser, { type: 'bearer' })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('User does not exist'));

                done();
            });
    });

    test('Missing authorization token', async done => {
        request(httpServer.app)
            .get('/me')
            .expect(401)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('Missing authorization token', ResponseCodeEnum.UNAUTHORIZED));

                done();
            });
    });

    test('Authorization token has expired', async done => {
        request(httpServer.app)
            .get('/me')
            .auth(tokenExpired, { type: 'bearer' })
            .expect(401)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('Authorization token has expired', ResponseCodeEnum.UNAUTHORIZED));

                done();
            });
    });
});
