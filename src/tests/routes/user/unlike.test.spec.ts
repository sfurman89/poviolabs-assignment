import request from 'supertest';
import jwt from 'jsonwebtoken';
import { errorResponse, successResponse } from '../../../middlewares/response';
import { ResponseCodeEnum } from '../../../enums/response-code.enum';
import { startWebServer } from '../../../web-server';

let httpServer;
let token = '';

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();

    const { sign } = jwt as jest.Mocked<typeof import('jsonwebtoken')>;

    token = sign({ id: 1 }, process.env.JWT_SECRET, {
        expiresIn: '24h'
    });

    await request(httpServer.app)
        .post('/user/4/like')
        .auth(token, { type: 'bearer' });
});

afterAll(async () => {
    await httpServer.close();
});

describe('User - API endpoint: GET /user/:id/unlike', () => {
    test('Id must be integer', async done => {
        request(httpServer.app)
            .delete('/user/id/unlike')
            .auth(token, { type: 'bearer' })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('Id must be integer'));

                done();
            });
    });

    test('You can not unlike yourself', async done => {
        request(httpServer.app)
            .delete('/user/1/unlike')
            .auth(token, { type: 'bearer' })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('You can not unlike yourself'));

                done();
            });
    });

    test('Successfully unliked', async done => {
        request(httpServer.app)
            .delete('/user/4/unlike')
            .auth(token, { type: 'bearer' })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(successResponse());

                done();
            });
    });

    test('You have not liked yet', async done => {
        request(httpServer.app)
            .delete('/user/5/unlike')
            .auth(token, { type: 'bearer' })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('You have not liked yet'));

                done();
            });
    });

    test('User does not exist', async done => {
        request(httpServer.app)
            .delete('/user/-1/unlike')
            .auth(token, { type: 'bearer' })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('User does not exist'));

                done();
            });
    });

    test('Missing authorization token', async done => {
        request(httpServer.app)
            .delete('/user/1/unlike')
            .expect(401)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('Missing authorization token', ResponseCodeEnum.UNAUTHORIZED));

                done();
            });
    });
});
