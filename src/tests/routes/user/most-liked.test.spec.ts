import request from 'supertest';
import jwt from 'jsonwebtoken';
import { successResponse } from '../../../middlewares/response';
import { startWebServer } from '../../../web-server';

let httpServer;
let token = '';

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();

    const { sign } = jwt as jest.Mocked<typeof import('jsonwebtoken')>;

    token = sign({ id: 1 }, process.env.JWT_SECRET, {
        expiresIn: '24h'
    });
});

afterAll(async () => {
    await httpServer.close();
});

describe('User - API endpoint: GET /most-liked', () => {
    test('List users in a most liked to least liked (with authorization token)', async done => {
        request(httpServer.app)
            .get('/most-liked')
            .auth(token, { type: 'bearer' })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(successResponse([{
                    username: 'user-2',
                    numberOfLikes: '1'
                }]));

                done();
            });
    });
    test('List users in a most liked to least liked (without authorization token)', async done => {
        request(httpServer.app)
            .get('/most-liked')
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(successResponse([{
                    username: 'user-2',
                    numberOfLikes: '1'
                }]));

                done();
            });
    });
});
