import request from 'supertest';
import jwt from 'jsonwebtoken';
import { errorResponse, successResponse } from '../../../middlewares/response';
import { ResponseCodeEnum } from '../../../enums/response-code.enum';
import { startWebServer } from '../../../web-server';

let httpServer;
let token = '';

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();

    const { sign } = jwt as jest.Mocked<typeof import('jsonwebtoken')>;

    token = sign({ id: 1 }, process.env.JWT_SECRET, {
        expiresIn: '24h'
    });
});

afterAll(async () => {
    await request(httpServer.app)
        .delete('/user/3/unlike')
        .auth(token, { type: 'bearer' });

    await httpServer.close();
});

describe('User like - API endpoint: GET /user/:id/like', () => {
    test('Id must be integer', async done => {
        request(httpServer.app)
            .post('/user/id/like')
            .auth(token, { type: 'bearer' })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('Id must be integer'));

                done();
            });
    });

    test('You can not like yourself', async done => {
        request(httpServer.app)
            .post('/user/1/like')
            .auth(token, { type: 'bearer' })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('You can not like yourself'));

                done();
            });
    });

    test('Successfully liked', async done => {
        request(httpServer.app)
            .post('/user/3/like')
            .auth(token, { type: 'bearer' })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(successResponse());

                done();
            });
    });

    test('You have already liked', async done => {
        request(httpServer.app)
            .post('/user/2/like')
            .auth(token, { type: 'bearer' })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('You have already liked'));

                done();
            });
    });

    test('User does not exist', async done => {
        request(httpServer.app)
            .post('/user/-1/like')
            .auth(token, { type: 'bearer' })
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('User does not exist'));

                done();
            });
    });

    test('Missing authorization token', async done => {
        request(httpServer.app)
            .post('/user/1/like')
            .expect(401)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                expect(JSON.parse(res.text)).toEqual(errorResponse('Missing authorization token', ResponseCodeEnum.UNAUTHORIZED));

                done();
            });
    });
});
