import { startWebServer } from '../../../web-server';
import { mostLiked } from '../../../controllers/user';

let httpServer;

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();
});

afterAll(async () => {
    await httpServer.close();
});

describe('User /most-liked', () => {
    test('List users in a most liked to least liked', async () => {
        await expect(await mostLiked()).toEqual([{
            username: 'user-2',
            numberOfLikes: '1'
        }]);
    });
});
