import jwt from 'jsonwebtoken';
import { IRequest } from '../../../interfaces/system.interface';
import { ExceptionHandler } from '../../../handlers/exception.handler';
import { startWebServer } from '../../../web-server';
import { Context } from '../../../context';
import { get } from '../../../controllers/user';

let httpServer;
let token = '';
let tokenNoUser;

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();

    const { sign } = jwt as jest.Mocked<typeof import('jsonwebtoken')>;

    token = `Bearer ${sign({ id: 1 }, process.env.JWT_SECRET, {
        expiresIn: '24h'
    })}`;

    tokenNoUser = `Bearer ${sign({ id: -1 }, process.env.JWT_SECRET, {
        expiresIn: '24h'
    })}`;
});

afterAll(async () => {
    await httpServer.close();
});

describe('User /me', () => {
    test('Username of logged in user', async () => {
        const request: Partial<IRequest> = {
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(await get(request as IRequest)).toEqual({
            username: 'user-1'
        });
    });

    test('User does not exist', async () => {
        const request: Partial<IRequest> = {
            context: await new Context(httpServer.stage),
            headers: {
                authorization: tokenNoUser
            }
        };

        await expect(get(request as IRequest)).rejects.toEqual(new ExceptionHandler('User does not exist'));
    });
});
