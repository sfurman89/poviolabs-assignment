import jwt from 'jsonwebtoken';
import { IRequest } from '../../../interfaces/system.interface';
import { ExceptionHandler } from '../../../handlers/exception.handler';
import { startWebServer } from '../../../web-server';
import { Context } from '../../../context';
import { like, unlike } from '../../../controllers/user';

let httpServer;
let token = '';

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();

    const { sign } = jwt as jest.Mocked<typeof import('jsonwebtoken')>;

    token = `Bearer ${sign({ id: 1 }, process.env.JWT_SECRET, {
        expiresIn: '24h'
    })}`;

    const request: Partial<IRequest> = {
        params: {
            id: '4'
        },
        context: await new Context(httpServer.stage),
        headers: {
            authorization: token
        }
    };

    await like(request as IRequest);
});

afterAll(async () => {
    await httpServer.close();
});

describe('User /unlike', () => {
    test('Id must be integer', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: 'id'
            },
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(unlike(request as IRequest)).rejects.toEqual(new ExceptionHandler('Id must be integer'));
    });

    test('You can not unlike yourself', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: '1'
            },
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(unlike(request as IRequest)).rejects.toEqual(new ExceptionHandler('You can not unlike yourself'));
    });

    test('Successfully unliked', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: '4'
            },
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(unlike(request as IRequest)).resolves.not.toThrow();
    });

    test('You have not liked yet', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: '5'
            },
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(unlike(request as IRequest)).rejects.toEqual(new ExceptionHandler('You have not liked yet'));
    });

    test('User does not exist', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: '-1'
            },
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(unlike(request as IRequest)).rejects.toEqual(new ExceptionHandler('User does not exist'));
    });
});
