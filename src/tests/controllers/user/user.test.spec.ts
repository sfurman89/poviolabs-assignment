import jwt from 'jsonwebtoken';
import { IRequest } from '../../../interfaces/system.interface';
import { ExceptionHandler } from '../../../handlers/exception.handler';
import { startWebServer } from '../../../web-server';
import { Context } from '../../../context';
import { numberOfLikes } from '../../../controllers/user';

let httpServer;
let token = '';

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();

    const { sign } = jwt as jest.Mocked<typeof import('jsonwebtoken')>;

    token = `Bearer ${sign({ id: 1 }, process.env.JWT_SECRET, {
        expiresIn: '24h'
    })}`;
});

afterAll(async () => {
    await httpServer.close();
});

describe('User /user/:id', () => {
    test('Id must be integer', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: 'id'
            },
            context: await new Context(httpServer.stage)
        };

        await expect(numberOfLikes(request as IRequest)).rejects.toEqual(new ExceptionHandler('Id must be integer'));
    });

    test('Invalid user id', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: '-1'
            },
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(numberOfLikes(request as IRequest)).rejects.toEqual(new ExceptionHandler('Invalid user id'));
    });

    test('List username & number of likes of a user', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: '1'
            },
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(await numberOfLikes(request as IRequest)).toEqual({
            username: 'user-1',
            numberOfLikes: 0
        });
    });
});
