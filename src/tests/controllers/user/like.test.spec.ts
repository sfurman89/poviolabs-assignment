import jwt from 'jsonwebtoken';
import { IRequest } from '../../../interfaces/system.interface';
import { ExceptionHandler } from '../../../handlers/exception.handler';
import { startWebServer } from '../../../web-server';
import { Context } from '../../../context';
import { like, unlike } from '../../../controllers/user';

let httpServer;
let token = '';

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();

    const { sign } = jwt as jest.Mocked<typeof import('jsonwebtoken')>;

    token = `Bearer ${sign({ id: 1 }, process.env.JWT_SECRET, {
        expiresIn: '24h'
    })}`;
});

afterAll(async () => {
    const request: Partial<IRequest> = {
        params: {
            id: '3'
        },
        context: await new Context(httpServer.stage),
        headers: {
            authorization: token
        }
    };

    await unlike(request as IRequest);
    await httpServer.close();
});

describe('User /like', () => {
    test('Id must be integer', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: 'id'
            },
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(like(request as IRequest)).rejects.toEqual(new ExceptionHandler('Id must be integer'));
    });

    test('You can not like yourself', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: '1'
            },
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(like(request as IRequest)).rejects.toEqual(new ExceptionHandler('You can not like yourself'));
    });

    test('Successfully liked', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: '3'
            },
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(like(request as IRequest)).resolves.not.toThrow();
    });

    test('You have already liked', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: '2'
            },
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(like(request as IRequest)).rejects.toEqual(new ExceptionHandler('You have already liked'));
    });

    test('User does not exist', async () => {
        const request: Partial<IRequest> = {
            params: {
                id: '-1'
            },
            context: await new Context(httpServer.stage),
            headers: {
                authorization: token
            }
        };

        await expect(like(request as IRequest)).rejects.toEqual(new ExceptionHandler('User does not exist'));
    });
});
