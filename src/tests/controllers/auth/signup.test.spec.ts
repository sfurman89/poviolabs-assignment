import { IRequest } from '../../../interfaces/system.interface';
import { ExceptionHandler } from '../../../handlers/exception.handler';
import { startWebServer } from '../../../web-server';
import { Context } from '../../../context';
import { signup } from '../../../controllers/auth';
import moment from 'moment';

let httpServer;

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();
});

afterAll(async () => {
    await httpServer.close();
});

describe('Auth /signup', () => {
    test('Creating new user', async () => {
        const request: Partial<IRequest> = {
            body: {
                username: `user${moment().unix()}`,
                password: 'test'
            },
            context: await new Context(httpServer.stage)
        };

        await expect(signup(request as IRequest)).resolves.not.toThrow();
    });

    test('User already exist', async () => {
        const request: Partial<IRequest> = {
            body: {
                username: 'user-1',
                password: 'test'
            },
            context: await new Context(httpServer.stage)
        };

        await expect(signup(request as IRequest)).rejects.toEqual(new ExceptionHandler('User already exist'));
    });

    test('Invalid request parameters', async () => {
        const request: Partial<IRequest> = {
            body: {
                username: 'user-1'
            },
            context: await new Context(httpServer.stage)
        };

        await expect(signup(request as IRequest)).rejects.toEqual(new ExceptionHandler('Invalid request parameters'));
    });
});
