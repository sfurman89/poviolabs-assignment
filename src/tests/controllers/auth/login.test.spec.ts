import jwt from 'jsonwebtoken';
import { IRequest } from '../../../interfaces/system.interface';
import { ExceptionHandler } from '../../../handlers/exception.handler';
import { startWebServer } from '../../../web-server';
import { login } from '../../../controllers/auth';
import { Context } from '../../../context';

let httpServer;

beforeAll(async () => {
    httpServer = await startWebServer();
    await httpServer.stage.connect();
});

afterAll(async () => {
    await httpServer.close();
});

describe('Auth /login', () => {
    test('Successfully logged in', async () => {
        const request: Partial<IRequest> = {
            body: {
                username: 'user-1',
                password: 'admin'
            },
            context: await new Context(httpServer.stage)
        };

        const { sign } = jwt as jest.Mocked<typeof import('jsonwebtoken')>;

        expect(await login(request as IRequest)).toEqual({
            token: sign({ id: 1 }, process.env.JWT_SECRET, {
                expiresIn: '24h'
            })
        });
    });

    test('Invalid username or password', async () => {
        const request: Partial<IRequest> = {
            body: {
                username: 'user-2',
                password: 'test'
            },
            context: await new Context(httpServer.stage)
        };

        await expect(login(request as IRequest)).rejects.toEqual(new ExceptionHandler('Invalid username or password'));
    });

    test('Invalid request parameters', async () => {
        const request: Partial<IRequest> = {
            body: {
                username: 'user-2'
            },
            context: await new Context(httpServer.stage)
        };

        await expect(login(request as IRequest)).rejects.toEqual(new ExceptionHandler('Invalid request parameters'));
    });
});
