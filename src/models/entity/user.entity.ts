import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
    name: 'user'
})
export class UserEntity {
    @PrimaryGeneratedColumn({
        type: 'integer',
        unsigned: true
    })
    id: number;

    @Column({
        name: 'username',
        length: 63
    })
    username: string;

    @Column({
        name: 'password',
        length: 32
    })
    password: string;

    @Column({
        name: 'timestamp',
        type: 'timestamp',
        onUpdate: 'CURRENT_TIMESTAMP',
        default: () => 'CURRENT_TIMESTAMP'
    })
    timestamp: string;
}
