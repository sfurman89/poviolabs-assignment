import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
    name: 'user_like'
})
export class UserLikeEntity {
    @PrimaryGeneratedColumn({
        type: 'integer',
        unsigned: true
    })
    id: number;

    @Column({
        name: 'user_id',
        unsigned: true
    })
    userId: number;

    @Column({
        name: 'user_id_like',
        unsigned: true
    })
    userIdLike: number;

    @Column({
        name: 'timestamp',
        type: 'timestamp',
        onUpdate: 'CURRENT_TIMESTAMP',
        default: () => 'CURRENT_TIMESTAMP'
    })
    timestamp: string;
}
