import { MigrationInterface, QueryRunner, Table } from 'typeorm';
import { UserLikeEntity } from '../entity/user-like.entity';

export class UserLikeMigration1607528754455 implements MigrationInterface {
    /**
     * @method up
     * @param {QueryRunner} queryRunner
     * @returns {Promise<void>}
     */
    async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'user_like',
            columns: [
                {
                    name: 'id',
                    type: 'integer',
                    unsigned: true,
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: 'user_id',
                    type: 'integer',
                    unsigned: true
                },
                {
                    name: 'user_id_like',
                    type: 'integer',
                    unsigned: true
                },
                {
                    name: 'timestamp',
                    type: 'timestamp',
                    onUpdate: 'CURRENT_TIMESTAMP',
                    default: 'CURRENT_TIMESTAMP'
                }
            ]
        }), true);

        if (process.env.ENVIRONMENT === 'test') {
            await queryRunner.connection.getRepository(UserLikeEntity).insert({
                userId: 1,
                userIdLike: 2
            });
        }
    }

    /**
     * @method down
     * @param {QueryRunner} queryRunner
     * @returns {Promise<void>}
     */
    async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('user_like');
    }
}
