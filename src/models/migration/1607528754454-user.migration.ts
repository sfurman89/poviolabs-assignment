import { MigrationInterface, QueryRunner, Table } from 'typeorm';
import md5 from 'md5';
import { UserEntity } from '../entity/user.entity';

export class UserMigration1607528754454 implements MigrationInterface {
    /**
     * @method up
     * @param {QueryRunner} queryRunner
     * @returns {Promise<void>}
     */
    async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'user',
            columns: [
                {
                    name: 'id',
                    type: 'integer',
                    unsigned: true,
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: 'username',
                    type: 'varchar',
                    length: '63'
                },
                {
                    name: 'password',
                    type: 'varchar',
                    length: '32'
                },
                {
                    name: 'timestamp',
                    type: 'timestamp',
                    onUpdate: 'CURRENT_TIMESTAMP',
                    default: 'CURRENT_TIMESTAMP'
                }
            ]
        }), true);

        if (process.env.ENVIRONMENT === 'test') {
            const password = md5(process.env.DB_TEST_USER_PASS);
            await queryRunner.connection.getRepository(UserEntity).insert([
                {
                    username: 'user-1',
                    password
                },
                {
                    username: 'user-2',
                    password
                },
                {
                    username: 'user-3',
                    password
                },
                {
                    username: 'user-4',
                    password
                },
                {
                    username: 'user-5',
                    password
                }
            ]);
        }
    }

    /**
     * @method down
     * @param {QueryRunner} queryRunner
     * @returns {Promise<void>}
     */
    async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('user');
    }
}
