import { HttpServer } from '.';
import { Stage } from './stage';
import envConfig from './config/env.config';
import databaseConfig from './config/database.config';
import defaultConfig from './config/default.config';

/**
 * @method startWebServer
 * @returns {Promise<HttpServer>}
 */
export const startWebServer = async (): Promise<HttpServer> => {
    const stage = new Stage(envConfig, databaseConfig, defaultConfig);
    const httpServer = new HttpServer(stage);

    try {
        await stage.connect();
        await httpServer.start();
    } catch (exception) {
        await stage.disconnect();
        await httpServer.close();
    }

    return httpServer;
};
