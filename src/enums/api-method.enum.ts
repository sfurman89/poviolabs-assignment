export enum ApiMethodEnum {
    POST   = 'post',
    GET    = 'get',
    PUT    = 'put',
    PATCH  = 'patch',
    DELETE = 'delete',
}
