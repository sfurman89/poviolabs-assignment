export enum ResponseCodeEnum {
    SUCCESS               = 200200,
    BAD_REQUEST           = 400400,
    UNAUTHORIZED          = 400401,
    PAYMENT_REQUIRED      = 400402,
    FORBIDDEN             = 400403,
    NOT_FOUND             = 400404,
    INTERNAL_SERVER_ERROR = 500500
}
