import { startWebServer } from '..';

(async () => {
    await startWebServer();
})();
