import { IEnvConfig } from './interfaces/env-config.interface';
import { Connection } from 'typeorm';
import { Stage } from './stage';

export class Context {
    public envConfig: IEnvConfig;
    public connection: Connection;

    /**
     * @constructor
     * @param {Stage} stage
     */
    constructor(stage: Stage) {
        this.envConfig = stage.envConfig;
        this.connection = stage.connection;
    }
}
