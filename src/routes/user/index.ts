import { IRequest, IResponse } from '../../interfaces/system.interface';
import { IRouteOption } from '../../interfaces/app.interface';
import { login, signup } from '../../controllers/auth';
import { get, like, mostLiked, numberOfLikes, unlike, updatePassword } from '../../controllers/user';
import { ApiMethodEnum } from '../../enums/api-method.enum';

export default [{
    path: '/signup',
    method: ApiMethodEnum.POST,
    auth: false,
    handler: async (req: IRequest, res: IResponse) => {
        await signup(req);

        res.success(null, 'User successfully created');
    }
}, {
    path: '/login',
    method: ApiMethodEnum.POST,
    auth: false,
    handler: async (req: IRequest, res: IResponse) => {
        res.success(await login(req));
    }
}, {
    path: '/me/update-password',
    method: ApiMethodEnum.PATCH,
    handler: async (req: IRequest, res: IResponse) => {
        res.success(await updatePassword(req));
    }
}, {
    path: '/me',
    method: ApiMethodEnum.GET,
    handler: async (req: IRequest, res: IResponse) => {
        res.success(await get(req));
    }
}, {
    path: '/user/:id/like',
    method: ApiMethodEnum.POST,
    handler: async (req: IRequest, res: IResponse) => {
        res.success(await like(req));
    }
}, {
    path: '/user/:id/unlike',
    method: ApiMethodEnum.DELETE,
    handler: async (req: IRequest, res: IResponse) => {
        res.success(await unlike(req));
    }
}, {
    path: '/user/:id',
    method: ApiMethodEnum.GET,
    auth: false,
    handler: async (req: IRequest, res: IResponse) => {
        res.success(await numberOfLikes(req));
    }
}, {
    path: '/most-liked',
    method: ApiMethodEnum.GET,
    auth: false,
    handler: async (req: IRequest, res: IResponse) => {
        res.success(await mostLiked());
    }
}] as IRouteOption[];
