import { IRoute } from '../interfaces/app.interface';
import userRoutes from './user';

export default [{
    path: '',
    routes: userRoutes
}] as IRoute[];
