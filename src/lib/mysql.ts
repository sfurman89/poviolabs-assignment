import { Connection, createConnection, getConnectionManager } from 'typeorm';
import { IDatabaseConfig } from '../interfaces/database-config.interface';
import { MainHelper } from '../helpers/main.helper';

/**
 * @function closeDatabaseConnection
 * @returns {Promise<void>}
 */
export const closeConnection = async () => {
    !getConnectionManager().has('default') || await getConnectionManager().get('default').close();
};

/**
 * @function getConnection
 * @param {IEnvConfig} databaseConfig
 * @returns {Promise<Connection>}
 */
export const getConnection = async (databaseConfig: IDatabaseConfig) => {
    if (getConnectionManager().has('default')) {
        return getConnectionManager().get('default');
    }

    try {
        return await createConnection({ ...databaseConfig as any });
    } catch (exception) {
        if (exception.name === 'AlreadyHasActiveConnectionError') {
            return getConnectionManager().get('default');
        } else {
            await MainHelper.Delay();

            return getConnection(databaseConfig);
        }
    }
};
