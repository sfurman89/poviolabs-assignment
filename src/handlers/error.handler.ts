import { Application } from 'express';
import { INextFunction, IRequest, IResponse } from '../interfaces/system.interface';
import { HTTP404Error, HTTPClientError } from '../utils/http-error';
import { ExceptionHandler } from './exception.handler';
import { errorResponse } from '../middlewares/response';
import { ResponseCodeEnum } from '../enums/response-code.enum';

/**
 * @function handle404Error
 * @param {Application} app
 */
const handle404Error = (app: Application) => {
    app.use(() => {
        throw new HTTP404Error(errorResponse('Method not found'));
    });
};

/**
 * @function handleClientError
 * @param {Application} app
 */
const handleClientError = (app: Application) => {
    app.use((err: Error, req: IRequest, res: IResponse, next: INextFunction) => {
        if (err instanceof HTTPClientError) {
            res.status(err.statusCode || 400).send(err.message);
        } else {
            next(err);
        }
    });
};

/**
 * @function handleServerError
 * @param {Application} app
 */
const handleServerError = (app: Application) => {
    app.use((err: Error, req: IRequest, res: IResponse, next: INextFunction) => {
        if (err instanceof ExceptionHandler) {
            res.status(err.statusCode).send(errorResponse(err.message, err.code));
        } else {
            res.status(500).send(errorResponse(err.message, ResponseCodeEnum.INTERNAL_SERVER_ERROR));
        }
    });
};

export default [handle404Error, handleClientError, handleServerError];
