import { Application, RequestHandler } from 'express';
import { INextFunction, IRequest, IResponse } from '../interfaces/system.interface';
import { Context, Stage } from '..';

/**
 * @function injectContext
 * @param {Application} app
 * @param {Stage} stage
 */
export const injectContext = (app: Application, stage: Stage): void => {
    app.use(_createContext(stage));
};

/**
 * @private
 * @function _createContext
 * @param {Stage} stage
 * @returns {Promise<void>}
 */
const _createContext = (stage: Stage): RequestHandler => async (req: IRequest, res: IResponse, next: INextFunction): Promise<void> => {
    req.context = await new Context(stage);

    next();
};
