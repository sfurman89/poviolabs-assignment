import { ResponseCodeEnum } from '../enums/response-code.enum';

export class ExceptionHandler {
    message: string;
    code: number;
    statusCode: number;

    /**
     * @constructor
     * @param {string} message
     * @param {number} code
     * @param {number} statusCode
     */
    constructor(message: string, code: number = ResponseCodeEnum.BAD_REQUEST, statusCode: number = 400) {
        this.message = message;
        this.code = code;
        this.statusCode = statusCode;
    }
}
