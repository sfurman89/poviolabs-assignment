import 'reflect-metadata';
import express from 'express';
import { Server } from 'http';
import middlewares from './middlewares';
import routes from './routes';
import errorHandlers from './handlers/error.handler';
import { injectContext } from './handlers/context.handler';
import { injectMiddlewares, injectRoutes } from './utils';
import { Stage } from './stage';

export class HttpServer {
    private _server: Server;

    readonly app: express.Application;
    readonly stage: Stage;

    /**
     * @constructor
     * @param {Stage} stage
     */
    public constructor(stage: Stage) {
        this.app = express();
        this.stage = stage;
        this._injectMiddlewares();
        this._injectContext();
        this._injectRoutes();
        this._injectErrorHandlers();
    }

    /**
     * @private
     * @method _injectMiddlewares
     */
    private _injectMiddlewares(): void {
        injectMiddlewares(middlewares, this.app);
    }

    /**
     * @private
     * @method _injectContext
     */
    private _injectContext(): void {
        injectContext(this.app, this.stage);
    }

    /**
     * @private
     * @method _injectRoutes
     */
    private _injectRoutes(): void {
        injectRoutes(routes, this.app);
    }

    /**
     * @private
     * @method _injectErrorHandlers
     */
    private _injectErrorHandlers(): void {
        injectMiddlewares(errorHandlers, this.app);
    }

    /**
     * @method start
     * @returns {Promise<void>}
     */
    public async start(): Promise<void> {
        this._server = await this.app.listen(this.stage.envConfig.http.port, this.stage.envConfig.http.host);
    }

    /**
     * @method close
     * @returns {Promise<void>}
     */
    public async close(): Promise<void> {
        !this._server || await this._server.close();

        this._server = undefined;
    }
}
