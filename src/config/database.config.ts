import { IDatabaseConfig } from '../interfaces/database-config.interface';

export = {
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    type: process.env.DB_TYPE,
    synchronize: false,
    logging: false,
    entities: [
        process.env.ORM_ENTITIES
    ],
    migrations: [
        process.env.ORM_MIGRATIONS
    ],
    subscribers: [
        process.env.ORM_SUBSCRIBERS
    ],
    cli: {
        'entitiesDir': 'dist/models/entity',
        'migrationsDir': 'dist/models/migration',
        'subscribersDir': 'dist/models/subscriber'
    }
} as IDatabaseConfig;
