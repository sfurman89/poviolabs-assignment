import { IDefaultConfig } from '../interfaces/default-config.interface';

export default {
    /* Swagger config */
    swagger: {
        definition: {
            openapi: '3.0.3',
            info: {
                title: 'POVIOLABS Assignment',
                description: 'REST Api for POVIOLABS Assignment',
                version: '1.0.0'
            }
        },
        options: {
            apis: ['**/docs/**/*.yaml']
        }
    }
} as IDefaultConfig;
