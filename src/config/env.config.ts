import { IEnvConfig } from '../interfaces/env-config.interface';

export default {
    /* HTTP server config */
    http: {
        host: process.env.HTTP_HOST || 'localhost',
        port: Number(process.env.HTTP_PORT || '9000')
    }
} as IEnvConfig;
