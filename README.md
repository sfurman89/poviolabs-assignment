# Poviolabs Assignment

I used MySQL because I am working with it most of the time. I have also experience with relational database postgres and noSQL.

# Usage
Explanation how to use and run app with docker or standalone.
## Using docker
Running tests
```bash
$ docker-compose -f docker-compose-test.yml up
```
or
```bash
$ npm run docker:test
```
Starting app
```bash
$ docker-compose up
```
or

```bash
$ npm run docker
```
> **Note:** You can not run **tests** and **app** at the same time. Before using another you have to stop docker with command **docker-compose down** or **npm run docker:down**

## Standalone
For development
```bash
$ npm run dev
```
For production
```bash
$ npm start
```

## API Docs
Url: [http://localhost:9000/api-docs](http://localhost:9000/api-docs)

